use std::convert::TryFrom;

#[derive(Debug, PartialEq, Clone)]
pub enum Instruction {
    Up(u32),
    Down(u32),
    Forward(u32),
}

impl Instruction {
    pub fn value(&self) -> u32 {
        match &self {
            Instruction::Up(value) => *value,
            Instruction::Down(value) => *value,
            Instruction::Forward(value) => *value,
        }
    }
}

impl TryFrom<String> for Instruction {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let parts: Vec<_> = value.split(" ").collect();
        let amount = parts[1].parse().map_err(|e| format!("Error: {}", e))?;
        match parts[0] {
            "up" => Ok(Instruction::Up(amount)),
            "down" => Ok(Instruction::Down(amount)),
            "forward" => Ok(Instruction::Forward(amount)),
            _ => Err("Booooo".to_string()),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Location {
    horizontal: u32,
    depth: u32,
    aim: i32,
}

impl Location {
    pub fn new(horizontal: u32, depth: u32, aim: i32) -> Self {
        Location {
            horizontal,
            depth,
            aim,
        }
    }

    // TODO: Use safer arithmatic
    pub fn update(self, instruction: &Instruction) -> Self {
        let mut horizontal = self.horizontal;
        let mut depth = self.depth;
        let mut aim = self.aim;
        match instruction {
            Instruction::Up(up) => aim -= *up as i32,
            Instruction::Down(down) => aim += *down as i32,
            Instruction::Forward(forward) => {
                horizontal += *forward;
                depth = (*forward as i32 * aim + depth as i32) as u32; // Yuck
            }
        }
        Location {
            horizontal,
            depth,
            aim,
        }
    }

    pub fn coords(&self) -> (u32, u32) {
        (self.horizontal, self.depth)
    }

    pub fn multiplied(&self) -> u32 {
        let (a, b) = self.coords();
        a * b
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn forward_from_string() {
        let actual: Instruction = "forward 10".to_string().try_into().unwrap();
        let expected = Instruction::Forward(10);
        assert_eq!(actual, expected);
    }

    #[test]
    fn up_from_string() {
        let actual: Instruction = "up 10".to_string().try_into().unwrap();
        let expected = Instruction::Up(10);
        assert_eq!(actual, expected);
    }

    #[test]
    fn down_from_string() {
        let actual: Instruction = "down 10".to_string().try_into().unwrap();
        let expected = Instruction::Down(10);
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_up() {
        let start = Location::new(0, 12, 0);
        let instruction = Instruction::Up(10);
        let actual = start.update(&instruction);
        let expected = Location::new(0, 12, -10);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_down() {
        let start = Location::new(0, 12, 0);
        let instruction = Instruction::Down(10);
        let actual = start.update(&instruction);
        let expected = Location::new(0, 12, 10);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_forward() {
        let start = Location::new(0, 12, 0);
        let instruction = Instruction::Forward(10);
        let actual = start.update(&instruction);
        let expected = Location::new(10, 12, 0);
        assert_eq!(expected, actual);
    }
}
