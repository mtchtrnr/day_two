use nav::{Instruction, Location};

use crate::file_reader::FileReader;

pub mod nav;

pub mod file_reader;

pub trait Reader {
    fn get_instructions(&self) -> Result<Vec<Instruction>, String>;
}

pub fn move_sub<R: Reader>(reader: R, start: Location) -> Result<Location, String> {
    Ok(reader
        .get_instructions()?
        .iter()
        .fold(start, |acc, instr| acc.update(&instr)))
}

fn main() {
    let reader = FileReader::new("data/day_two.txt".to_string());
    let start = Location::new(0, 0, 0);
    let final_location = move_sub(reader, start).unwrap();
    println!("Final Location: {:?}", final_location);
    println!("Answer: {}", final_location.multiplied())
}

#[cfg(test)]
mod tests {
    use super::*;

    struct TestReader {
        instructions: Vec<Instruction>,
    }

    impl TestReader {
        pub fn new(instructions: Vec<Instruction>) -> Self {
            TestReader { instructions }
        }
    }

    impl Reader for TestReader {
        fn get_instructions(&self) -> Result<Vec<Instruction>, String> {
            Ok(self.instructions.clone())
        }
    }

    #[test]
    fn test_move_sub() {
        let instructions = vec![
            Instruction::Forward(5),
            Instruction::Down(5),
            Instruction::Forward(8),
            Instruction::Up(3),
            Instruction::Down(8),
            Instruction::Forward(2),
        ];
        let reader = TestReader::new(instructions);
        let start = Location::new(0, 0, 0);
        let expected = 900;
        let actual = move_sub(reader, start).unwrap().multiplied();
        assert_eq!(expected, actual);
    }
}
