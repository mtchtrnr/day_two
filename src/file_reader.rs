use std::fmt::Debug;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use crate::nav::Instruction;
use crate::Reader;

pub struct FileReader {
    path: String,
}

impl FileReader {
    pub fn new(path: String) -> Self {
        FileReader { path }
    }
}

impl Reader for FileReader {
    fn get_instructions(&self) -> Result<Vec<crate::nav::Instruction>, String> {
        string_lines(&self.path)?
            .into_iter()
            .map(TryFrom::try_from)
            .collect()
    }
}

fn string_lines(filename: &str) -> Result<Vec<String>, String> {
    read_lines(filename)
        .map_err(error_to_string)?
        .map(|res| res.map_err(error_to_string))
        .collect()
}

fn error_to_string<E: Debug>(e: E) -> String {
    format!("Error: {:?}", e)
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_from_file() {
        let path = "src/file_reader/test_data.txt".to_string();
        let reader = FileReader { path };
        let actual = reader.get_instructions().unwrap();
        let expected = vec![
            Instruction::Forward(5),
            Instruction::Down(5),
            Instruction::Forward(8),
            Instruction::Up(3),
            Instruction::Down(8),
            Instruction::Forward(2),
        ];
        assert_eq!(actual, expected);
    }
}
